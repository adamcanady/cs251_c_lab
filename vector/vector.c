#import "vector.h"
#import <stdio.h>
#import <stdlib.h>

/* Function prototypes. */

/* Take an already existing Vector as a parameter. (Do not create a Vector
 inside this function. Set its internal memory size to match the value given in
 the parameter, allocate an appropriately-sized C array to hold the actual data,
 and initialize the size variable to 0. */

void init(Vector *vector, int memorySize){
	vector->array = (int *)malloc(sizeof(int)*memorySize);
	vector->memorySize = memorySize;
	vector->size = 0;
}


/* Removes the array associated with the Vector. */

void cleanup(Vector *vector){
	free(vector->array);
	vector->memorySize = 0;
	vector->size = 0;
}


/* Print out the Vector for debugging purposes. */

void print(Vector *vector){
	printf("The vector is of size: %i\n", vector->size);
	printf("The vector's memory size is %i\n", vector->memorySize);
	printf("The elements in the vector are: ");
	for(int i = 0; i < vector->size; i++){
		printf("%i ", vector->array[i]);
	}
	printf("\n");
}


/* Insert value at location inside the Vector. If there is already data at that
 location, insert slides the rest of the data down one position to make room for
 the new value. Returns 1 if success, and 0 if the location is invalid (less
 than 0, or greater than the size). Inserting at the very end of the current
 data (at position equal to size) is fine, and should increase size by 1.

 If the internal array is too small to fit a new value, a new array of twice the
 size is created with malloc. The values are copied, the Vector is appropriately
 updated, and the old array is freed.) */

int insert(Vector *vector, int location, int value){
	vector->size++; //increment the size
	// double if necessary
	if(vector->size >= vector->memorySize){
		int *newarray = (int *) malloc(sizeof(int) * (vector->memorySize * 2)); // create new array of double the size
		
		// copy values
		for (int i = 0; i < vector->size; i++){
			newarray[i] = vector->array[i];
		}

		free(vector->array); // free the old array
		vector->array = newarray; // change pointer to new array

		vector->memorySize = vector->memorySize*2;
	}

	if(location >= vector->size || location < 0){
		return 0;
	}
	// if something is already in that location
	if(location >= 0 && location < vector->size){
		// slide the rest down
		int current = vector->array[location]; // get what's blocking our path
		for(int i = location; i < vector->size; i++){
			int next = vector->array[i+1];
			vector->array[i+1] = current;
			current = next;
		}
	}
	// and insert
	vector->array[location] = value;
	return 1;
}





/* Obtains value at location inside the Vector. Returns 1 if success, and 0 if
 the location is invalid (less than 0, or greater than or equal to the
 size). The value itself is returned through the parameter value. */

int get(Vector *vector, int location, int *value){
	if(vector->size > location){ //if this is a location within the array
		*value = vector->array[location];
		return 1;
	} else {
		return 0;
	}
}


/* Deletes value at location inside the Vector.  Returns 1 if success, and 0 if
 the location is invalid (less than 0, or greater than or equal to the
 size). When an item is deleted, everything else past it in the array should be
 slid down. The internal array itself does not need to be compressed, i.e., you
 don't need to halve it in size if it becomes half full, or anything like
 that. */

int delete(Vector *vector, int location){
	if(location > vector->size || location < 0){ // validate location
		return 0;
	}

	// if something is already in that location
	if(vector->array[location]){

		// slide the rest up
		for(int i = location; i <= vector->size; i++){
			vector->array[i] = vector->array[i+1]; //get the next value and put it in the current spot
		}
		vector->array[vector->size-1] = 0;
	}
	vector->size--; //increment the size
	return 1;
}



