/* Temperature.c converts between F and C */

#include <stdio.h>
#include <stdlib.h>

int main() {
	float temp_f;
	printf("What is the temperature in degrees Fahrenheit? ");
	int ret = scanf("%f", &temp_f);
	if(!ret){
		printf("Please only enter number values.\n");
		exit(1);
	}

	float temp_c = (temp_f - 32) * 5/9;

	printf("%f degrees Fahrenheit is %f degrees Celsius.\n", temp_f, temp_c);

}