// complex.c by Max Willard and Adam Canady

// This file defines a struct Complex that conatins real and imaginary
// values that represent a complex number.

// The program later defines a function multiply_complex that multiplies
// imaginary numbers. The main function tests this function and prints outs
// a result.

#include <stdio.h>

struct Complex {
	double real;
	double imaginary;
};

struct Complex multiply_complex(struct Complex c1, struct Complex c2){
	struct Complex answer;
	answer.real = c1.real * c2.real - c1.imaginary * c2.imaginary;
	answer.imaginary = c1.real * c2.imaginary + c2.real * c1.imaginary;

	return answer;
}

int main(){
	struct Complex c1;
	c1.real = 2;
	c1.imaginary = 3;
	struct Complex c2;
	c2.real = 5;
	c2.imaginary = 8;

	struct Complex result = multiply_complex(c1, c2);

	printf("The real part of the result is: %f, and the imaginary part is: %f", result.real, result.imaginary);
}