// stack.c by Max Willard and Adam Canady
// CS 251 - Dave Musicant - W2014

// This program illustrates that the stack is growing "down" by
// using a recursive program that prints out a local variable's 
// pointer at each level. When run it shows that the variable's 
// memory location decrements on each allocation, implying that
// the stack is growing down.

#include <stdio.h>

int recursive(int n){
	if(n == 0){
		return 0;
	}

	int a;
	printf("n is: %i and the local variable a is: %p\n", n, &a);

	return recursive(n-1) + 1;
}

int main(){
	int b = recursive(3);
}