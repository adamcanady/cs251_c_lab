#include <stdlib.h>
#include <time.h>
#include <stdio.h> 
#include <math.h>

int main(){
	srandom(time(NULL));
	long num = random();
	int computer_guess = 100 * num / (pow((long) 2, 31)-1);
	// printf("Computer guess %i\n", computer_guess);

	int user_guess;
	printf("I'm thinking of a number between 1 and 100. Please enter your guess: ");
	while(1){
		int ret = scanf("%i", &user_guess);
		if(!ret){
			printf("Please only enter number values.\n");
			exit(1);
		}

		if(user_guess == computer_guess){
			printf("Yay! You win!\n");
			break;
		} else if (user_guess < computer_guess) {
			printf("Too Low! Try another number: ");
		} else {
			printf("Too High! Try another number: ");
		}
	}
}

